"""
Setup script for SUMM project, Andrei Lupasco
@paarruti
"""

from setuptools import setup, find_packages

setup(
    name="rf_opti_sps",
    version="0.1",
    packages=find_packages(),

    # Project details
    #author="P",
    #author_email="your.email@example.com",
    #description="A brief description of your project",
    #long_description=open('README.md').read(),
    #long_description_content_type='text/markdown',
    #url="http://your.project.url",

    # Dependencies
    install_requires=[
        # List your project dependencies here, e.g.,
        "numpy",
        "pandas",
        "matplotlib",
        "scipy",
        "cpymad",

        #"blond",  # Not required for now
        #"xsuite", # Not required for now

    ],

    # Package classifiers
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],

    python_requires='>=3.6',
)
