# Optimisation of RF techniques for improved slow extraction from the Super Proton Synchrotron


Experimental users in the North Area receive long spills of particles from the CERN Super Proton Synchrotron via resonant slow extraction. Recent R&D efforts have demonstrated that Radio Frequency (RF) techniques have a large potential to substantially improve the quality of such spills. However, the modelling and optimisation of RF techniques can be challenging and computationally expensive. In this project, the student will develop an effective approach that exploits machine-learning frameworks to investigate RF techniques relevant to slow extraction, e.g. empty-bucket channelling. In particular, a surrogate model will be built to speed up the evaluation of control techniques, and finally different numerical methods will be tested to maximise spill quality.

## TODO

- [ ] Pablo: Obtain experimentally measured beam moment spread for SFTPRO
- [ ] Pablo: Obtain correct parameters for SFTPRO bunch rotation
- [ ] Andrei: Download repo, install requirements (requirements.txt + setup.py)
- [ ] Andrei: Run `basic_physics_simulator.ipynb`
- [ ] Andrei: Implement double-harmonic bunch rotation. How does it impact the main observables?

## Resources

### Logistics

- [Section technical quickstart](https://gitlab.cern.ch/abt-optics-and-code-repository/misc/quickstart)

### Theory/Concepts:

Physics keywords: `Synchrotrons`, `Longitudinal beam dynamics`, `RF manipulations`
ML keywords: `Surrogate models`, `Global optimisers`, `Local optimisers`, `Physics-inspired ML`


- Lecture on "Intro to particle accelerators":https://cds.cern.ch/record/2229757
- Lectures on "Longitudinal beam dynamics":
    - https://cds.cern.ch/record/2235809?ln=en
    - https://cds.cern.ch/record/2235753?ln=en
- Lectures on "RF manipulations":
    - https://cds.cern.ch/record/2882194?ln=en
    - RF manipulations II: https://cds.cern.ch/record/2882196?ln=en
- An intro textbook to particle accelerators: [Wilson](https://academic.oup.com/book/11694/chapter/160647274)
- A more advanced textbook on particle accelerators: [Wiedemann](https://link.springer.com/book/10.1007/978-3-319-18317-6)
- Details of equations of longitudinal beam dynamics: [BLonD](https://blond-admin.github.io/BLonD/modules/equations_of_motion.html)
- (Quite) complete accelerator simulator: [Xsuite](https://xsuite.readthedocs.io/en/latest/)

### Papers

- [RF phase displacement in PS](https://cds.cern.ch/record/2886026/)


### Works at CERN:
- [Example of git project with surrogate model](https://gitlab.cern.ch/sps-projects/case_teca)


## Studies

### Basic physics simulator



